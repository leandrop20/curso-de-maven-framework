package br.com.devmedia.maven.eclipse;

import java.util.LinkedList;
import java.util.List;

/**
 * class elevator.
 * @author PINHEIRO
 *
 */
public class Elevador {

	/**
	 * list persons.
	 */
	private final List<Pessoa> pessoas = new LinkedList<Pessoa>();
	/**
	 * max persons.
	 */
	private final int MAX_PESSOAS = 8;
	
	public List<Pessoa> getPessoas() {
		return pessoas;
	}
	
	public void addPessoa(Pessoa a) {
		if (pessoas.size() >= MAX_PESSOAS) {
			throw new IllegalArgumentException("N�mero m�ximo de pessoas no elevador");
		}
		pessoas.add(a);
	}
	
	public void removePessoas(Pessoa a) {
		pessoas.remove(pessoas.indexOf(a));
	}
	
}