package br.com.devmedia.maven.eclipse;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestClass {

	public static void main(String[] args) throws ParseException {
		System.out.println("Elevador Iniciado");
		Elevador elevador = new Elevador();
		
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setIdade(33);
		pessoa1.setName("Diego Souza");
		pessoa1.setNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("12/01/1982"));
		pessoa1.setEndereco("Endere�o 1");
		
		Pessoa pessoa2 = new Pessoa();
		pessoa2.setIdade(31);
		pessoa2.setName("Jo�o Moreno");
		pessoa2.setNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("12/12/2000"));
		pessoa2.setEndereco("Endere�o 2");
		
		elevador.addPessoa(pessoa1);
		elevador.addPessoa(pessoa2);
		
		System.out.println("Elevador Finalizado");
	}
	
}