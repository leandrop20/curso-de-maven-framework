package br.com.devmedia.maven.eclipse;

import java.io.Serializable;
import java.util.Date;

/**
 * Entity person.
 * @author PINHEIRO
 * 
 */
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * name.
	 */
	private String name;
	/**
	 * age.
	 */
	private int idade;
	/**
	 * address.
	 */
	private String endereco;
	/**
	 * birth.
	 */
	private Date nascimento;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(final int idade) {
		this.idade = idade;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(final String endereco) {
		this.endereco = endereco;
	}

	public Date getNascimento() {
		return nascimento;
	}

	public void setNascimento(final Date nascimento) {
		this.nascimento = nascimento;
	}

}