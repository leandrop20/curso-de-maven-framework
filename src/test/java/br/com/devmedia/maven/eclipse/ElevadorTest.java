package br.com.devmedia.maven.eclipse;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElevadorTest extends Elevador {

	private Elevador elevador;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("Iniciando Teste...");
		elevador = new Elevador();
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Finalizando Teste...");
		elevador = null;
	}

	@Test
	public void testGetPessoas() {
		System.out.println("Testing testGetPessoas()");
		assertTrue(elevador.getPessoas().isEmpty());
		Pessoa pes1 = new Pessoa();
		pes1.setIdade(22);
		pes1.setName("Jo�o");
		pes1.setEndereco("Endereco Jo�o");
		elevador.addPessoa(pes1);
		assertTrue(elevador.getPessoas().size() == 1);
		elevador.removePessoas(pes1);
		assertTrue(elevador.getPessoas().isEmpty());
	}

	@Test
	public void testAddPessoa() {
		System.out.println("Testing testAddPessoa()");
		Pessoa pes1 = new Pessoa();
		pes1.setIdade(22);
		pes1.setName("Jo�o");
		pes1.setEndereco("Endereco Jo�o");
		elevador.addPessoa(pes1);
		assertTrue(elevador.getPessoas().size() == 1);
	}

	@Test
	public void testRemovePessoas() {
		System.out.println("Testing testRemovePessoa()");
		Pessoa pes1 = new Pessoa();
		pes1.setIdade(22);
		pes1.setName("Jo�o");
		pes1.setEndereco("Endereco Jo�o");
		elevador.addPessoa(pes1);
		assertTrue(elevador.getPessoas().size() == 1);
		Pessoa pes2 = new Pessoa();
		pes2.setIdade(42);
		pes2.setName("Jo�o 22");
		pes2.setEndereco("Endereco Joao 22");
		elevador.addPessoa(pes2);
		assertTrue(elevador.getPessoas().size() == 2);
		elevador.removePessoas(pes1);
		assertTrue(elevador.getPessoas().size() == 1);
		elevador.removePessoas(pes2);
		assertTrue(elevador.getPessoas().isEmpty());
	}

}
